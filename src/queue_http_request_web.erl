-module(queue_http_request_web).

-export([start_link/1, stop/0, loop/1]).

start_link({port, Port}) ->
  start_link([{ip, "0.0.0.0"}, {port, Port}, {backlog, 200}]);

start_link(Options) ->
  Loop = fun(Req) ->
    ?MODULE:loop(Req)
  end,
  MochiwebHttpOptions = proplists:delete(docroot, Options),

  mochiweb_http:start([{max, 1000000}, {name, ?MODULE}, {loop, Loop} | MochiwebHttpOptions]).

stop() ->
  mochiweb_http:stop(?MODULE).

loop(Req) ->
  do_handle(Req).

do_handle(Req) ->
  QueryParameters = extract_query_parameters(Req:parse_qs()),
  QueryData = extract_query_data(Req:get(method), Req),
  send_query(Req, QueryParameters, QueryData).

extract_query_parameters(ReqData) ->
  Url = proplists:get_value("url", ReqData, undefined),
  Cooldown = list_to_integer(proplists:get_value("cooldown", ReqData, "0")),
  {Url, Cooldown}.

extract_query_data('GET', _Req) ->
  {get};
extract_query_data('POST', Req) ->
  {post, Req:recv_body()}.

send_query(Req, {undefined, _}, _) ->
  response(Req, {error, parameter_url_is_not_found});
send_query(Req, {RawUrl, Cooldown}, {get}) ->
  Url = http_uri:decode(RawUrl),
  UpdatedHeaders = update_headers(Req:get(headers), Url),
  response(Req, queue_http_request_queue:request({Url, UpdatedHeaders, get}, {Cooldown}));
send_query(Req, {RawUrl, Cooldown}, {post, PostData}) ->
  Url = http_uri:decode(RawUrl),
  UpdatedHeaders = update_headers(Req:get(headers), Url),
  response(Req, queue_http_request_queue:request({Url, UpdatedHeaders, post, PostData}, {Cooldown})).

update_headers(RequestHeaders, Url) ->
  change_host_in_headers(normalize_headers(RequestHeaders), get_host(Url)).

normalize_headers(Headers) ->
  RawHeaders = mochiweb_headers:to_list(Headers),
  [normalize_header(RawHeader) || RawHeader <- RawHeaders].

normalize_header({Key, Value}) when is_atom(Key) ->
  {atom_to_list(Key), Value};
normalize_header({Key, Value}) ->
  {Key, Value}.

get_host(Url) ->
  lists:nth(2, string:tokens(Url, "/")).

change_host_in_headers(Headers, Host) ->
  orddict:store("Host", Host, orddict:from_list(Headers)).

response(Req, {ok, {Status, Headers, Body}}) ->
  Req:respond({Status, Headers, Body});
response(Req, {error, Reason}) ->
  Req:respond({502, [], "<h1>Invalid request</h1><p>Code error: " ++ atom_to_list(Reason) ++ "</p>"}).
