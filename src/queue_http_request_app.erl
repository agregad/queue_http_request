-module(queue_http_request_app).

-behaviour(application).

%% Application callbacks
-export([start/2, stop/1]).

%% ===================================================================
%% Application callbacks
%% ===================================================================

start(_StartType, _StartArgs) ->
    queue_http_request_sup:start_link().

stop(_State) ->
    ok.
