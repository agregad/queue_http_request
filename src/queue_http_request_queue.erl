-module(queue_http_request_queue).

-behaviour(gen_server).

%% API
-export([start_link/0]).

%% gen_server callbacks
-export([init/1,
  handle_call/3,
  handle_cast/2,
  handle_info/2,
  terminate/2,
  code_change/3]).

-export([
  request/2
]).

-define(SERVER, ?MODULE).

-record(state, {}).


%%%===================================================================
%%% API
%%%===================================================================

%%--------------------------------------------------------------------
%% @doc
%% Starts the server
%%
%% @end
%%--------------------------------------------------------------------
start_link() ->
  inets:start(),
  gen_server:start_link({local, ?SERVER}, ?MODULE, [], []).

%%%===================================================================
%%% gen_server callbacks
%%%===================================================================
request({Url, Headers, Method}, Options) ->
  request({Url, Headers, Method, undefined}, Options);
request({Url, Headers, Method, Body}, Options) ->
  gen_server:call(?SERVER, {request, {{Url, Headers, Method, Body}, Options}}, infinity).

%%--------------------------------------------------------------------
%% @private
%% @doc
%% Initializes the server
%%
%% @spec init(Args) -> {ok, State} |
%%                     {ok, State, Timeout} |
%%                     ignore |
%%                     {stop, Reason}
%% @end
%%--------------------------------------------------------------------
-spec(init(Args :: term()) ->
  {ok, State :: null} | {ok, State :: null, timeout() | hibernate} |
  {stop, Reason :: term()} | ignore).
init([]) ->
  {ok, #state{}}.

%%--------------------------------------------------------------------
%% @private
%% @doc
%% Handling call messages
%%
%% @end
%%--------------------------------------------------------------------
-spec(handle_call(Request :: term(), From :: {pid(), Tag :: term()},
    State :: #state{}) ->
  {reply, Reply :: term(), NewState :: #state{}} |
  {reply, Reply :: term(), NewState :: #state{}, timeout() | hibernate} |
  {noreply, NewState :: #state{}} |
  {noreply, NewState :: #state{}, timeout() | hibernate} |
  {stop, Reason :: term(), Reply :: term(), NewState :: #state{}} |
  {stop, Reason :: term(), NewState :: #state{}}).

handle_call({request, {{Url, _Headers, Method, _Body}=Request, {Cooldown}}}, _From, State) ->
  timer:sleep(Cooldown),
  RawResponse = do_request(Request),
  Reply = format_response(RawResponse),
  log(Url, Method, Reply, Cooldown),
  {reply, Reply, State};

handle_call(_Request, _From, State) ->
  {reply, ok, State}.

%%--------------------------------------------------------------------
%% @private
%% @doc
%% Handling cast messages
%%
%% @end
%%--------------------------------------------------------------------
-spec(handle_cast(Request :: term(), State :: #state{}) ->
  {noreply, NewState :: #state{}} |
  {noreply, NewState :: #state{}, timeout() | hibernate} |
  {stop, Reason :: term(), NewState :: #state{}}).

handle_cast(_Request, State) ->
  {noreply, State}.

%%--------------------------------------------------------------------
%% @private
%% @doc
%% Handling all non call/cast messages
%%
%% @spec handle_info(Info, State) -> {noreply, State} |
%%                                   {noreply, State, Timeout} |
%%                                   {stop, Reason, State}
%% @end
%%--------------------------------------------------------------------
-spec(handle_info(Info :: timeout() | term(), State :: #state{}) ->
  {noreply, NewState :: #state{}} |
  {noreply, NewState :: #state{}, timeout() | hibernate} |
  {stop, Reason :: term(), NewState :: #state{}}).
handle_info(_Info, State) ->
  {noreply, State}.

%%--------------------------------------------------------------------
%% @private
%% @doc
%% This function is called by a gen_server when it is about to
%% terminate. It should be the opposite of Module:init/1 and do any
%% necessary cleaning up. When it returns, the gen_server terminates
%% with Reason. The return value is ignored.
%%
%% @spec terminate(Reason, State) -> void()
%% @end
%%--------------------------------------------------------------------
-spec(terminate(Reason :: (normal | shutdown | {shutdown, term()} | term()),
    State :: #state{}) -> term()).
terminate(_Reason, _State) ->
  ok.

%%--------------------------------------------------------------------
%% @private
%% @doc
%% Convert process state when code is changed
%%
%% @spec code_change(OldVsn, State, Extra) -> {ok, NewState}
%% @end
%%--------------------------------------------------------------------
-spec(code_change(OldVsn :: term() | {down, term()}, State :: #state{},
    Extra :: term()) ->
  {ok, NewState :: #state{}} | {error, Reason :: term()}).
code_change(_OldVsn, State, _Extra) ->
  {ok, State}.

%%%===================================================================
%%% Internal functions
%%%===================================================================
do_request({undefined, _, _, _}) ->
  {send_request_error, url_is_undefined};
do_request({Url, Headers, get, undefined}) ->
  {send_request_ok, httpc:request(get, {Url, Headers}, [], [])};
do_request({Url, Headers, post, undefined}) ->
  {send_request_ok, httpc:request(post, {Url, Headers, "application/x-www-form-urlencoded", ""}, [], [])};
do_request({Url, Headers, post, Body}) ->
  {send_request_ok, httpc:request(post, {Url, Headers, "application/x-www-form-urlencoded", Body}, [], [])}.

format_response({send_request_ok, {ok, {{_, HttpStatusCode, _}, Headers, Body}}}) ->
  {ok, {HttpStatusCode, Headers, Body}};
format_response({send_request_ok, {error, {ErrorReason, _}}}) ->
  {error, ErrorReason};
format_response({send_request_error, url_is_undefined}) ->
  {error, url_is_undefined}.

log(Url, Method, {ok, {HttpStatus, _, _}}, Cooldown) ->
  adz_logger:log(Url ++ " [" ++ atom_to_list(Method) ++ "] " ++ integer_to_list(Cooldown) ++ " - " ++ integer_to_list(HttpStatus));
log(Url, Method, {error, Reason}, Cooldown) ->
  adz_logger:log(Url ++ " [" ++ atom_to_list(Method) ++ "] " ++ integer_to_list(Cooldown) ++ " - " ++ atom_to_list(Reason)).
