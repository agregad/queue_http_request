-module(queue_http_request_sup).

-behaviour(supervisor).
-include("queue_http_request.hrl").

%% API
-export([start_link/0]).

%% Supervisor callbacks
-export([init/1]).

%% Helper macro for declaring children of supervisor
-define(CHILD(I, Type), {I, {I, start_link, []}, permanent, 5000, Type, [I]}).

%% ===================================================================
%% API functions
%% ===================================================================

start_link() ->
    supervisor:start_link({local, ?MODULE}, ?MODULE, []).

%% ===================================================================
%% Supervisor callbacks
%% ===================================================================

init([]) ->
    {ok, { {one_for_one, 5, 10}, [
        {queue_http_request_queue, {queue_http_request_queue, start_link, []}, permanent, brutal_kill, worker, [queue_http_request_queue]},
        {adz_logger, {adz_logger, start_link, [get_app_dir() ++ "logs/app.log"]}, permanent, brutal_kill, worker, [adz_logger]},
        {queue_http_request_web, {queue_http_request_web, start_link, [{port, ?PORT}]}, permanent, brutal_kill, worker, [queue_http_request_web]}
    ]} }.

get_app_dir() ->
    {ok, Path} = file:get_cwd(),
    Path ++ "/".
