#!/bin/bash

erl -name conn_$1@$2 \
    -setcookie ABC \
    -remsh $1@$2
