#!/bin/bash

./bin/compile.sh

APPLICATION="queue_http_request"
NAME="httpqueue"

erl +P 16777216 \
    -env ERL_MAX_ETS_TABLES 15000 \
    -pa ebin/ ext/ deps/*/ebin/ \
    -name $NAME@$1 \
    -setcookie ABC \
    -config config/$2 \
    -boot start_sasl \
    -eval "application:start($APPLICATION)." \
    -detached
