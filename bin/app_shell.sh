#!/bin/bash

./bin/compile.sh

APPLICATION="queue_http_request"
NAME="httpqueue"
HOST=`hostname -I | cut -d' ' -f1`
ENV=$1

erl +P 16777216 \
    -env ERL_MAX_ETS_TABLES 15000 \
    -pa ebin/ ext/ deps/*/ebin/ \
    -name $NAME@$HOST \
    -setcookie ABC \
    -config config/$ENV \
    -boot start_sasl \
    -eval "application:start($APPLICATION)."
