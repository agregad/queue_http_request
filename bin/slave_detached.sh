#!/bin/bash

erl +P 16777216 \
    -env ERL_MAX_ETS_TABLES 15000 \
    -pa ebin/ ext/ deps/*/ebin/ \
    -name $1@$2 \
    -setcookie ABC \
    -detached
