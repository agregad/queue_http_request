#!/bin/bash

erl -name etop_$1@$2 \
    -setcookie ABC \
    -remsh $1@$2 \
    -eval "etop:start([{node, '$1@$2'},{interval,1},{tracing, off},{sort, memory}])."

#erl -name etop@192.168.1.39 -noinput -s etop -s erlang halt -output text \
#    -setcookie ABC -interval 1 -lines 20 -accumulate true > etop.log
